//
//  ViewController.swift
//  Cafe Finder
//
//  Created by Reggie Manuel Cabalo on 05/01/2018.
//  Copyright © 2018 Reggie Manuel Cabalo. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import GooglePlacesSearchController
import GooglePlaces
import Alamofire
import KRProgressHUD


class ViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {
    @IBOutlet weak var placeCollection: UICollectionView!
    
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    
    var numberOfRows = 0
    var arrayPlaceName:[String]!
    var arrayImage:[UIImage]!
    var arrayAddress:[String]!
    var arrayDistance:[AnyObject]!
    
    
    
    let currentLocation = CLLocationManager()
    var placesClient: GMSPlacesClient!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        currentLocation.delegate = self
        currentLocation.requestWhenInUseAuthorization()
        currentLocation.startUpdatingLocation()
        currentLocation.startMonitoringSignificantLocationChanges()
        
        arrayImage = []
        arrayAddress = []
        arrayDistance = []
        arrayPlaceName = []
        
        self.loadCafe()
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        // Put the search bar in the navigation bar.
        searchController?.searchBar.sizeToFit()
        navigationItem.titleView = searchController?.searchBar
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        // Prevent the navigation bar from being hidden when searching.
        searchController?.hidesNavigationBarDuringPresentation = false
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.loadCafe()
    }
    
    @IBAction func refresh(_ sender: Any) {
        self.loadCafe()
    }
    func loadImageForMetadata(photoMetadata: GMSPlacePhotoMetadata) {
        GMSPlacesClient.shared().loadPlacePhoto(photoMetadata, callback: {
            (photo, error) -> Void in
            if let error = error {
                // TODO: handle the error.
                print("Error: \(error.localizedDescription)")
            } else {
                self.arrayImage.append(photo!)
                self.placeCollection.reloadData()
            }
        })
    }
    
    func loadCafe() {
        KRProgressHUD.show()
        placesClient = GMSPlacesClient.shared()
        placesClient.currentPlace { (place, error) in
            KRProgressHUD.dismiss()
            if let place = place {
                for likelihood in place.likelihoods {
                    for types in likelihood.place.types {
                        if types == "cafe" {
                            let place = likelihood.place
                            print("Current Place name \(place.name) at likelihood \(likelihood.likelihood)")
                            print("Current Place address \(place.formattedAddress!)")
                            print("Current Place attributions \(place.attributions)")
                            print("Current PlaceID \(place.placeID)")
                            
                            self.arrayPlaceName.append(place.name)
                            self.arrayAddress.append(place.formattedAddress!)
                            
                            GMSPlacesClient.shared().lookUpPhotos(forPlaceID: place.placeID, callback: { (photos, error) in
                                if let firstPhoto = photos?.results.first {
                                    self.loadImageForMetadata(photoMetadata: firstPhoto)
                                }
                            })
                        }
                    }
                }
                
                if self.arrayPlaceName.count == 0 {
                    let alert = UIAlertController(title: "", message: "No cafe's found nearby.", preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "OK",
                                                     style: .cancel, handler: nil)
                    
                    alert.addAction(cancelAction)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayPlaceName.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.placeCollection.dequeueReusableCell(withReuseIdentifier: "places", for: indexPath) as! PlacesCollectionViewCell
        
        cell.imageView.image = self.arrayImage[indexPath.row]
        cell.cafeTitle.text = self.arrayPlaceName[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "PlaceDetailViewController") as! PlaceDetailViewController
        detailVC.cafeNameString =  self.arrayPlaceName[indexPath.row]
        detailVC.cafeAddressString = self.arrayAddress[indexPath.row]
        detailVC.image = self.arrayImage[indexPath.row]
        detailVC.lat = self.currentLocation.location?.coordinate.latitude
        detailVC.long = self.currentLocation.location?.coordinate.longitude
        self.navigationController?.pushViewController(detailVC, animated: true)
        
        print("pressed")
        
    }
    
}

extension ViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        // Do something with the selected place.
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

