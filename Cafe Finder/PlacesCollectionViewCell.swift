//
//  PlacesCollectionViewCell.swift
//  Cafe Finder
//
//  Created by Reggie Manuel Cabalo on 05/01/2018.
//  Copyright © 2018 Reggie Manuel Cabalo. All rights reserved.
//

import UIKit

class PlacesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var cafeTitle: UILabel!
    
}
