//
//  PlaceDetailViewController.swift
//  Cafe Finder
//
//  Created by Reggie Manuel Cabalo on 05/01/2018.
//  Copyright © 2018 Reggie Manuel Cabalo. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps


class PlaceDetailViewController: UIViewController {

    @IBOutlet weak var imagePlace: UIImageView!
    @IBOutlet weak var cafeName: UILabel!
    @IBOutlet weak var cafeAddress: UILabel!
    
    @IBOutlet weak var map: UIView!
    var cafeNameString: String!
    var cafeAddressString: String!
    var image: UIImage!
    
    var placeId: String!
    var lat: Double!
    var long: Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 10)
        let mapview = GMSMapView.map(withFrame: .zero, camera: camera)
        mapview.isMyLocationEnabled = true
        self.map = mapview
        
        let marker = GMSMarker()
        marker.position =  CLLocationCoordinate2D(latitude: lat, longitude: long)
        marker.title = "Your location"
        marker.map = mapview
        
        cafeName.text =  cafeNameString
        cafeAddress.text =  cafeAddressString
        imagePlace.image = image
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
